﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Logout : MonoBehaviour {

	//Access the Player Manager
	private PlayerManager pm;

	// Use this for initialization
	void Start () {
		pm = FindObjectOfType <PlayerManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void loadLevel (string levelName){
		SceneManager.LoadScene (levelName);
	}

	public void logout(){
		PlayerPrefs.DeleteKey ("Username");
		PlayerPrefs.DeleteKey ("Proficiency");
		PlayerPrefs.DeleteKey ("Displayname");
		PlayerPrefs.DeleteKey ("Optin");
		if (pm != null)
			Destroy (pm.gameObject);
		loadLevel ("main");
	}
}
