﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class ScorePanel : MonoBehaviour {
	//ScorePanel
	public GameObject scorePanel;

	//Url
	//public string urlRetrievePersonal;
	private string urlRetrievePanel;
	private string levelName;
	//public string levelHover;
	private string displayName;
	private string grizzName;

	//Player Name
	//private PlayerManager playerManager;

	//Level Name
	//private IdentifyLevel identifyLevel;

	//Player Stats
	/*string acc;
	string accRank;
	string time;
	string spdRank;*/
	string highScore;

	//Text and Images to display the stats

	/*public Text username;
	public Text score;
	public Image rankAcc;
	public Sprite[] medals;
	public Image rankSpd;*/
	public TextMeshProUGUI highScoreText;
	public TextMeshProUGUI levelNameText;
	private int maxPage;
	private int curPage;


	//Bool to slow down the number of times the website is requested
	private bool canCallCoroutine = true;

	// Use this for initialization
	void Start () {
		//Get the Player's display name
		//playerManager = FindObjectOfType<PlayerManager>();

		//grizzName = playerManager.getUserName();
		//displayName = playerManager.getDisplayName();

		//Get the name of the button's level
		//identifyLevel = FindObjectOfType<IdentifyLevel>();

		//Hide all tooltips
		//scorePanel.gameObject.SetActive (false);

		//levelHover = transform.parent.name;



		/*if (levelHover != null) {
			getLevelName ();
		} else {
			Debug.Log ("No level name found");
		}*/

		//username.text = displayName;

		//levelName = levelName;

		//urlRetrievePersonal = "https://ilta.oakland.edu/retrievePersonal.php?level=" + levelName + "&user=" + grizzName; //Main Server.
		urlRetrievePanel = "https://ilta.oakland.edu/retrievefull.php?level=" + levelName; //Main Server.
		levelNameText.text = levelName;

		//urlRetrievePersonal = "goeteeks.x10host.com/Tutorial/retrievePersonal.php?level=" + levelName + "&user=" + grizzName; //Backup Server.
		//urlRetrievePanel = "goeteeks.x10host.com/Tutorial/retrievePanel.php?level=" + levelName; //Backup Server.

		if (highScore == null){
			//score.text = "Loading Personal Score and Time";
			//score.alignment = TextAnchor.MiddleCenter;
			highScoreText.text = "Loading High Scores...";
			highScoreText.alignment = TextAlignmentOptions.Center;
		}



	}

	// Update is called once per frame
	void Update () {
		
		if (canCallCoroutine) {
			//StartCoroutine (retrievePersonal ());
			StartCoroutine (retrievePanel ());
			//score.text = "Score: " + acc + "\n\nTime: " + time;
			//score.alignment = TextAnchor.UpperLeft;
			//getMedalSprite ();
			highScoreText.text = highScore;
			highScoreText.alignment = TextAlignmentOptions.TopLeft;
		}

	}
	//Get score, ranks, and time for the current level highlighted by mouse.
	/*IEnumerator retrievePersonal(){
		//Slow down the number of calls
		canCallCoroutine = false;
		yield return new WaitForSeconds (1);


		//Get Accuracy Score
		string retrieveAccuracy = urlRetrievePersonal + "&column=Accuracy";
		WWW wwwRetrieveAccuracy = new WWW (retrieveAccuracy);
		yield return wwwRetrieveAccuracy;
		acc = wwwRetrieveAccuracy.text;
		//print (levelName + ": " + acc);

		//Get Accuracy Rank
		string retrieveAccRank = urlRetrievePersonal + "&column=AccuracyRank";
		WWW wwwRetrieveAccRank = new WWW (retrieveAccRank);
		yield return wwwRetrieveAccRank;
		accRank = wwwRetrieveAccRank.text;
		//print (levelName + ": " + accRank);

		//Get Time
		string retrieveTime = urlRetrievePersonal + "&column=Time";
		WWW wwwRetrieveTime = new WWW (retrieveTime);
		yield return wwwRetrieveTime;
		time = wwwRetrieveTime.text;
		//print (levelName + ": " + time);

		//Get Time Rank
		string retrieveSpdRank = urlRetrievePersonal + "&column=SpeedRank";
		WWW wwwRetrieveSpdRank = new WWW (retrieveSpdRank);
		yield return wwwRetrieveSpdRank;
		spdRank = wwwRetrieveSpdRank.text;
		//print (levelName + ": " + spdRank);
		canCallCoroutine = true;
	}*/

	//Get the top 3 scores
	IEnumerator retrievePanel(){
		canCallCoroutine = false;
		yield return new WaitForSeconds (1);
		WWW wwwRetrievePanel = new WWW (urlRetrievePanel);
		yield return wwwRetrievePanel;
		highScore = "<mspace=1em>" + wwwRetrievePanel.text + "</mspace>";
		maxPage = highScoreText.textInfo.pageCount;
		canCallCoroutine = true;
	}

	string getLevelName(string nameOfLevel){
		switch (nameOfLevel) {
		case "Armbone1":
			return "Large Bone Whole Arm";
		case "Armbone2":
			return "Bony Structure Arm";
		case "Armbone3":
			return "Bony Structure Forearm & Hand";
		case "Nerves1":
			return "Nervous System Arm";
		case "Nerves2":
			return "Nerves Skin Arm";
		case "Nerves3":
			return "Post-Test 3 Nerves";
		case "UpperArm1":
			return "Arm Locations";
		case "UpperArm2":
			return "Arm Naming";
		case "UpperArm3":
			return "Arm Attachments";
		case "UpperArm4":
			return "Arm Innervation";
		case "UpperArm5":
			return "Arm Action";
		case "UpperArm6":
			return "Post-Test 1 Arm";
		case "Forearm1A":
			return "Forearm Anterior Locations";
		case "Forearm2A":
			return "Forearm Anterior Naming";
		case "Forearm3A":
			return "Forearm Anterior Attachments";
		case "Forearm4A":
			return "Forearm Anterior Innervation";
		case "Forearm5A":
			return "Forearm Anterior Action";
		case "Forearm1B":
			return "Forearm Posterior Locations";
		case "Forearm2B":
			return "Forearm Posterior Naming";
		case "Forearm3B":
			return "Forearm Posterior Attachments";
		case "Forearm4B":
			return "Forearm Posterior Innervation";
		case "Forearm5B":
			return "Forearm Posterior Action";
		case "Forearm6":
			return "Post-Test 4 Forearm";
		case "Hand1A":
			return "Hand Locations";
		case "Hand2A":
			return "Hand Naming";
		case "Hand3A":
			return "Hand Attachments";
		case "Hand4A":
			return "Hand Innervation";
		case "Hand5A":
			return "Hand Action";
		case "Hand6A":
			return "Post-Test 2 Hand";
		default:
			return "";
		}
	}
		
	//Set the Image of the medal by rank
	/*void getMedalSprite(){
		//Accuracy Rank gets the blue banner medals
		switch (accRank) {
		case "Bronze":
			rankAcc.gameObject.SetActive(true);
			rankAcc.sprite = medals [5];
			break;
		case "Silver":
			rankAcc.gameObject.SetActive(true);
			rankAcc.sprite = medals [6];
			break;
		case "Gold":
			rankAcc.gameObject.SetActive(true);
			rankAcc.sprite = medals [7];
			break;
		case "Platinum":
			rankAcc.gameObject.SetActive(true);
			rankAcc.sprite = medals [8];
			break;
		default:
			rankAcc.gameObject.SetActive (false);
			break;
		}

		//Speed Rank gets the yellow banner medals
		switch (spdRank) {
		case "Bronze":
			rankSpd.gameObject.SetActive(true);
			rankSpd.sprite = medals [10];
			break;
		case "Silver":
			rankSpd.gameObject.SetActive(true);
			rankSpd.sprite = medals [11];
			break;
		case "Gold":
			rankSpd.gameObject.SetActive(true);
			rankSpd.sprite = medals [12];
			break;
		case "Platinum":
			rankSpd.gameObject.SetActive(true);
			rankSpd.sprite = medals [13];
			break;
		default:
			rankSpd.gameObject.SetActive(false);
			break;
		}
	}*/

	//Show the score panel (when mouse is hovering over the level button)
	/*public void showScorePanal(){
		//if (Application.platform != RuntimePlatform.Android && Application.platform != RuntimePlatform.IPhonePlayer) {
			scorePanel.gameObject.SetActive (true);
			canCallCoroutine = true;
		//}
	}*/

	//Hide the score panel (when mouse is not hovering over any level buttons)
	/*public void hideScorePanel(){
		scorePanel.gameObject.SetActive (false);
		canCallCoroutine = true;
	}*/

	public void callPanel (string nameOfLevel){
		levelName = nameOfLevel;
		levelNameText.text = getLevelName(nameOfLevel);
		urlRetrievePanel = "https://ilta.oakland.edu/retrievefull.php?level=" + levelName; //Main Server.
		canCallCoroutine = true;
		curPage = 1;
		highScoreText.pageToDisplay = curPage;
	}

	public void nextPage (){
		if (curPage < maxPage) {
			highScoreText.pageToDisplay = ++curPage;
		} else {
			//curPage = 1;
			//highScoreText.pageToDisplay = curPage;
		}
	}

	public void prevPage (){
		if (curPage > 1) {
			highScoreText.pageToDisplay = --curPage;
		} else {
			//curPage = maxPage;
			//highScoreText.pageToDisplay = maxPage;
		}
	}
}