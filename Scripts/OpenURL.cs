﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using TMPro;

//Opens an URL using the TextMeshPro's link tags.

public class OpenURL : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {

	private Camera m_Camera; //The camera for raycasting purposes
	private int m_selectedLink = -1; //The current link the mouse is clicked on/hovered over
	private TextMeshProUGUI proText; //The text field the link is contained in
	private PlayerManager playerManager;

	// Use this for initialization
	void Start () {
		m_Camera = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Camera> (); //Find the camera
		proText = GetComponent<TextMeshProUGUI> (); //Find the text field
		playerManager = FindObjectOfType<PlayerManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//OnPointerDown and OnPointerUp are needed to make OnPointerClick work
	public void OnPointerDown (PointerEventData eventData){
	
	}

	//OnPointerDown and OnPointerUp are needed to make OnPointerClick work
	public void OnPointerUp (PointerEventData eventData){
	
	}

	//User clicks over a hyperlink
	public void OnPointerClick (PointerEventData eventData)
	{
		//Debug.Log ("Click Position: " + eventData.position);  //Prints the position of the mouse pointer

		int linkIndex = TMP_TextUtilities.FindIntersectingLink (proText, Input.mousePosition, m_Camera); //Finds the current link the mouse is on top of

		//Clears any previous link data
		if ((linkIndex == -1 && m_selectedLink != -1) || linkIndex != m_selectedLink)
		{
			m_selectedLink = -1;
		}

		//Player clicks on a hyperlink text
		if (linkIndex != -1 && linkIndex != m_selectedLink) {
			m_selectedLink = linkIndex; //Sets the index of the link to global variable

			TMP_LinkInfo linkInfo = proText.textInfo.linkInfo [linkIndex]; //Gets the hash code of the link's index

			int linkHashCode = linkInfo.hashCode; //Sets the hash code of the link's index
			Debug.Log (linkHashCode); //Uncomment this to get the hash code of the link's index, and use it in the switch statement below

			//Figure out which link we are using and act accordingly
			switch (linkHashCode)
			{
			case 1652429227: //site  //The hash code of the link index 'site'
				//Debug.Log (linkHashCode + " Pressed");
				Application.OpenURL ("https://ilta.oakland.edu/register.html"); //Opens the website in the user's default browser
				break;
			case -1149794812:
				//Debug.Log (linkHashCode + " Pressed");
				Application.OpenURL ("https://ilta.oakland.edu/pwrecovery.html?email=" + playerManager.getUserName ());
				break;
			case 149375838:
				//Debug.Log (linkHashCode + " Pressed");
				Application.OpenURL ("https://oakland.az1.qualtrics.com/jfe/form/SV_9zcj787D950Peg5");
				break;
			default:
				Debug.LogWarning ("Hash not found for this link.  Uncomment line 58 --> Debug.Log (linkHashCode); --> to get the appropriate hash code.");
				break;
			}

		}
	}
}
