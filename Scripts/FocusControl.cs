﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FocusControl : MonoBehaviour {
	public InputField[] fields;
	public Button[] submitButton;
	//private bool[] selected;

	// Use this for initialization
	void Start () {
	/*	selected = new bool[submitButton.Length];
		for (int i = 0; i < submitButton.Length; i++) {
			selected [i] = false;
		}*/
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Tab)) {
			if ((Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift))) {
				selectReverseField ();
			} else {
				selectField ();
			}
		}



		if (Input.GetKeyDown (KeyCode.Return)) {
			print ("return pressed.");
			submittingButton ();
		}
	}

	void selectField(){
		for (int n = 0; n < fields.Length - 1; n++) {
			if (fields [n].isFocused) {
				fields [++n].Select ();
				//disableOtherButtons (submitButton.Length);
			} else if (fields [fields.Length - 1].isFocused /*|| isButtonSelected()*/) {
				for (int j = 0; j < submitButton.Length; j++) {
					if (submitButton [j].isActiveAndEnabled) {
						submitButton [j].Select ();
						//selected [j] = true;
						//disableOtherButtons (j);
						return;
					}
				}

			} else {
				fields [0].Select ();
				//disableOtherButtons (submitButton.Length);
			}
		}
	}

	void selectReverseField(){
		for (int n = fields.Length - 1; n > 0; n--) {
			if (fields [n].isFocused) {
				fields [--n].Select ();
			} else {
				fields [fields.Length - 1].Select ();
			}
		}
	}

	void submittingButton (){
		for (int i = 0; i < submitButton.Length; i++) {
			if (submitButton[i].isActiveAndEnabled) {
				print ("Calling Button.");
				submitButton[i].onClick.Invoke ();
				return;
			}
		}
	}

	/*void disableOtherButtons (int c){
		for (int i = 0; i < selected.Length; i++) {
			if (i != c) {
				selected [i] = false;
			}
		}
	}*/

	/*bool isButtonSelected(){
		for (int i = 0; i < submitButton.Length - 1; i++) {
			if (selected [i]) {
				return true;
			}
		}
		return false;
	}*/
}
