﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CanvasSelector : MonoBehaviour {

	//private Canvas boneCan;
	//private Canvas muscleCan;
	//private Canvas nervousCan;
	public GameObject[] canvases;
	private int curIndex;

	// Use this for initialization
	void Start () {
		//boneCan = GameObject.Find ("Bone Canvas").GetComponent <Canvas> (); 
		//muscleCan = GameObject.Find ("Muscle Canvas").GetComponent <Canvas> ();
		//nervousCan = GameObject.Find ("Nervous Canvas").GetComponent <Canvas> ();

		//boneCan.gameObject.SetActive (true);
		//muscleCan.gameObject.SetActive (false);
		//nervousCan.gameObject.SetActive (false);

		canvases [0].gameObject.SetActive (true);

		if (canvases.Length > 1) {
			for (int i = 1; i < canvases.Length; i++) {
				canvases [i].gameObject.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/*public void boneUp(){
		boneCan.gameObject.SetActive (true);
		muscleCan.gameObject.SetActive (false);
		//nervousCan.gameObject.SetActive (false);
	}*/

	/*public void muscleUp(){
		muscleCan.gameObject.SetActive (true);
		boneCan.gameObject.SetActive (false);
		//nervousCan.gameObject.SetActive (false);
	}*/

	/*public void nerveUp(){
		nervousCan.gameObject.SetActive (true);
		boneCan.gameObject.SetActive (false);
		muscleCan.gameObject.SetActive (false);
	}*/

	public void setIndex(int index){
		curIndex = index;
		setCanvas ();
	}

	public void setCanvas (){
		for (int i = 0; i < canvases.Length; i++) {
			canvases [i].gameObject.SetActive (false);
		}
		canvases [curIndex].gameObject.SetActive (true);
	}
}
